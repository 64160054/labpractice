package com.paowaric.week8;

public class Circle {
    //Attributes
    private double radian;

    public Circle(int radian) { // Construtor
        this.radian = radian;
    }
    public int printCircleRadian() {
        double area = 3.14 * (radian*radian);
        System.out.println(area);
        return (int)area;
    }
    public int printCirclePerimeter() {
        double perimeter = 2*3.14 ;
        System.out.println(perimeter); return (int)perimeter;
    }
}
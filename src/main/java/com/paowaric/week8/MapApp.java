package com.paowaric.week8;

public class MapApp {
    public static void main(String[] args) {
        MapShape map1 = new MapShape();
        map1.printMap();

        MapShape map2 = new MapShape(10,10);
        map2.printMap();
    }
}

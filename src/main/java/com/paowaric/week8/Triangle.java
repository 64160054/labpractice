package com.paowaric.week8;

public class Triangle {
    // Attributes
    private double a;
    private double b;
    private double c;

    public Triangle(double a, double b, double c) { // constructor
       this.a = a;
       this.b = b;
       this.c = c;
    }

    // Methodes
   public double printAreaTriangle() {
       double s = (a+b+c)/2;
       double area = Math.sqrt(s*(s-a)*(s-b)*(s-c));
       System.out.println("Calculate area" + " = " + area);
       return area;
   }

   public double printPerimeterTriangle() {
       double perimeter = a+b+c;
       System.out.println("Calculate perimeter" + " = " + perimeter);
       return perimeter;
   }
}
